import copy
from sage.groups.perm_gps.permgroup import PermutationGroup
from sage.groups.perm_gps.permgroup_named import CyclicPermutationGroup
from sage.combinat.permutation import Permutation

"""
A group action for a group of permutations of [16] acting on a set of
k-colorings of a 4x4 grid. The coloring is represented as a list of colors.
Color c at index i represents the i:th grid cell (left to right, top to bottom)
being colored with color c.
s.[a_1, ..., a_16] = [a_s(1), ..., a_s(16)]
"""
def act(perm, coloring):
	return [coloring[perm(i+1)-1] for i in range(len(coloring))]

"""
Returns the orbits for the given group acting on the given set with 
the given group action.
"""
def orbits(group, acted_set, action):
	# Copy the set that is acted on so that this function is guaranteed
	# to not be desctructive
	set_copy = copy.deepcopy(acted_set)

	result= []
	while set_copy:
		x = set_copy.pop()
		subresult = [x]

		# x and y are in the same orbit if there is a g in the group such that
		# g.x = y (g^(-1).y = x)
		# This means that for any element x in the set, the orbit of x can be
		# calculated by applying all elements g in the group to x with 
		# the group action
		for g in group:
			y = action(g, x)
			if y not in subresult:
				subresult.append(y)
				set_copy.remove(y)
		result.append(subresult)
	return result

"""
Returns all possible colorings of n elements with the given colors.
E.g. if n = 2 and colors = ['r', 'b'] then the output is
[['r', 'r'], ['b', 'b'], ['r', 'b'], ['b', 'r']]
"""
def get_colorings(colors, n = 16):
	# Base case: the empty coloring of 0 elements
	if n == 0:
		return [[]]
	
	# Recursive case: calculate all colorings of n-1 elements
	# Then for each such coloring, make new colorings by making a copy
	# for each color and adding the color to the copy
	subresult = get_colorings(colors, n - 1)
	result = []
	for coloring in subresult:
		for color in colors:
			result.append(coloring + [color])
	return result

"""
Returns the group of cyclic permutations of the columns of an n x n grid 
represented as a list of n^2 elements. Every grid cell is given a number 1..n 
from top left going row for row. E.g. if n=4 then the grid is:
1  2  3  4
5  6  7  8
9  10 11 12
13 14 15 16
An example cyclic permutation of columns would be 
(1,2,3,4)(5,6,7,8)(9,10,11,12)(13,14,15,16) which is written as the list 
p = [2, 3, 4, 1, 6, 7, 8, 5, 10, 11, 12, 9, 14, 15, 16, 13] where grid cell 
i is sent to grid cell p[i].
"""
def get_cyclic_col_perms(n=4):
	CyclicPerms = CyclicPermutationGroup(n)
	col_perms = []
	for s in CyclicPerms:
		col_perm = Permutation([s(i)+n*r for r in range(0,n) for i in range(1,n+1)])
		col_perms.append(col_perm)
	ColPerms = PermutationGroup(col_perms)
	return ColPerms

"""
Returns the group of cyclic permutations of the columns and rows at the same 
time of an n x n grid. See get_cyclic_col_perms().
"""
def get_cyclic_col_row_perms(n=4):
	col_perms = []
	row_perms = []

	# Use the cyclic permutations of n elements to construct cyclic elements
	# of the n columns and of the n rows
	CyclicPerms = CyclicPermutationGroup(n)
	for s in CyclicPerms:
		col_perm = Permutation([s(i)+n*r for r in range(0,n) for i in range(1,n+1)])
		col_perms.append(col_perm)
		row_perm = Permutation([i-n+n*s(r) for r in range(1,n+1) for i in range(1,n+1)])
		row_perms.append(row_perm)

	# Compose the cyclic permutations of columns and rows to create all possible
	# cyclic permutations of columns and rows at the same time 
	col_row_perms = []
	for c in col_perms:
		for r in row_perms:
			col_row_perms.append(c*r)
	ColRowPerms = PermutationGroup(col_row_perms)
	return ColRowPerms

"""
Returns the group corresponding to all permutations of dihedral symmetry
of an n x n grid, i.e. the group Dn.
For e.g. n=4 the grid is 
1  2  3  4
5  6  7  8
9  10 11 12
13 14 15 16
a rotation is (1,13,16,4)(2,9,15,8)(3,5,14,12)(6,10,11,7)
-> [13, 9, 5, 1, 14, 10, 6, 2, 15, 11, 7, 3, 16, 12, 8, 4]
a reflection is (1,13)(2,14)(3,15)(4,16)(5,9)(6,10)(7,11)(8,12)
-> [13, 14, 15, 16, 9, 10, 11, 12, 5, 6, 7, 8, 1, 2, 3, 4]
"""
def get_dihedral_perms(n=4):

    # A rotation of 360/n degrees and a reflection is enough to generate 
    # the whole dihedral group
	Dihedral = PermutationGroup([
	    # A rotation 90 degrees counter-clockwise corresponds to every row i being
	    # sent to column i in reverse (bottom to top)
        # I.e. go column for column (left to right) and go through each column 
        # in reverse (bottom to top)
        # column i in reverse starts with element n(n-1)+i. The element above 
        # is n(n-1)+i-n. The one above that is n(n-1)+i-2*n etc.
		Permutation([i-n*j for i in range(n**2-(n-1), n**2+1) for j in range(0, n)]),
        

	    # A reflection in the x-axis of the grid simply corresponds to enumerating
	    # The cells row for row from the bottom to the top
        # I.e. go through row n-1 to row 0. Row i starts with element n*i+1 and 
        # ends with element n*i+n 
		Permutation([n*r+c for r in range(n-1, -1, -1) for c in range(1, n+1)])
	])
	return Dihedral

def solve_exercise(n=4):
	# Construct all possible colorings of the nxn grid
	#Colors = ['r', 'b']
	Colors = ['r', 'b']
	Colorings = get_colorings(Colors, n**2)

	# Calculate the equivalent colorings under cyclic permutations of the columns
	# of the grid, i.e. the orbits of the group of cyclic permutations of the 
	# columns acting on the grid
	ColPerms = get_cyclic_col_perms(n)
	column_orbits = orbits(ColPerms, Colorings, act)
	print('equivalent colorings of cyclic permutations of columns --------------------------')
	print(len(column_orbits))
	#for orbit in column_orbits:
		#print(f'orbit: {orbit[0]}, {len(orbit)} elements')
		#print()

	# Calculate the equivalent colorings under cyclic permutations of the columns 
	# and rows of the grid at the same time.
	# This corresponds once again to orbits with the group action defined previously
	ColRowPerms = get_cyclic_col_row_perms(n)
	col_row_orbits = orbits(ColRowPerms, Colorings, act)
	print('equivalent colorings of cyclic permutations of columns and rows --------------------------')
	print(len(col_row_orbits))
	#for orbit in col_row_orbits:
		#print(f'orbit: {orbit[0]}, {len(orbit)} elements')
		#print()

	# Calculate and print the equivalent colorings under dihedral symmetry.
	Dihedral = get_dihedral_perms(n)
	dihedral_orbits = orbits(Dihedral, Colorings, act)
	print('equivalent colorings of dihedral permutations -------------------------------------------')
	print(len(dihedral_orbits))

solve_exercise(4)
