from sage.groups.perm_gps.permgroup import PermutationGroup
from sage.groups.perm_gps.permgroup_named import SymmetricGroup
from sage.combinat.subset import Subsets

"""
Returns True if the sets (lists) A and B are disjoint, False elsewise.
"""
def disjoint(A, B):
    for a in A:
        for b in B:
            if a == b:
                return False
    return True

"""
Returns the set X for the given n, i.e. the list of all ways to partition
[N] (N = 2n) into 2 disjoint sets of equal size.
"""
def get_X(n):
    N = 2*n
    N_subsets = Subsets(N, n)
    X = []
    for i, A in enumerate(N_subsets):
        for j in range(i+1, len(N_subsets)):
            B = N_subsets[j]
            if disjoint(A, B):
                X.append([A, B])
    return X

"""
Returns True if two sets {A1, ..., An} and {B1, …, Bn} are not equal.
Ai and Bj are sorted sets.
This assumes that both sets are sorted.
"""
def not_equal(A, B):
    for i in range(len(A)):
        A1 = A[i]
        B1 = B[i]
        for j in range(len(A1)):
            if A1[j] != B1[j]:
                return True
    return False 

"""
Returns the resulting element s.{A, B} for the group action
s.{A, B} = {s.A, S.B} where s is included in S_[N] and {A, B} is in X.
The resulting element is sorted.
"""
def act(sigma, X_elem):
    result = []
    for Set in X_elem:
        Set_result = []
        for elem in Set:
            Set_result.append(sigma(elem))
        result.append(Set_result)
    result_sorted = sorted([sorted(e) for e in result])
    return result_sorted

"""
Returns the list of all elements s of SX (Symmetric group of the set X) 
such that s.X = X where the group action is the one defined by act()
and equality is checked using not_equal().
"""
def ultra_fixed_points(SX, X):
    result = []
    for sigma in SX:
        # Assume that sigma is such a permutation that we seek
        fixed_point = True

        # Look for an element x in X such that s.x != x
        for x in X:
            if not_equal(act(sigma, x), x):
                fixed_point = False
                break

        # If there is no x such that s.x != x then sigma is such a permutation
        # that we seek :)
        if fixed_point:
            result.append(sigma)
    return result

"""
Calculate and print the group K described in the hand-in for a given n
as well as the quotient group SN/K.
"""
def get_group_k(n):
    N = 2*n
    SN = SymmetricGroup(N)
    X = get_X(n)
    X_sorted = [sorted(list([sorted(list(f)) for f in e])) for e in sorted(X)]
    K = ultra_fixed_points(SN, X_sorted)
    K_perm_group = PermutationGroup(K)
    QuotientGroup = SN.quotient(K_perm_group)
    print("X: ", X_sorted)
    print("K: ", K)
    print("Structure of K: ", K_perm_group.structure_description())
    print("Structure of SN/K: ", QuotientGroup.structure_description())

    
print("K for n=2:   --------------------------------------")
get_group_k(2)
print()

print("K for n=3:   --------------------------------------")
get_group_k(3)
print()
